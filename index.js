const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const cors = require('cors')

const port = 3100;
const Analyzer = require('natural').SentimentAnalyzer;
const stemmer = require('natural').PorterStemmer;
const analyzer = new Analyzer("English", stemmer, "afinn");
// getSentiment expects an array of strings
// console.log(analyzer.getSentiment(["I", "love", "cherries"]));
app.use(bodyParser.json())
app.use(cors());
app.listen(port, () => {
    console.log("listening on port", port);
})

app.post('/getanswer', (req,resp)=>{
    let itemBody = req.body.body;
    let result = itemBody.split(" ");
    let analyzedResult = analyzer.getSentiment(result)
    // console.log(analyzedResult)
    resp.send({status:"success", 
            message:analyzedResult
        })
})